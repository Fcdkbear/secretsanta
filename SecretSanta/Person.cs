﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretSanta
{
    class Person
    {
        public string email;
        public string name;

        public Person(string email, string name)
        {
            this.email = email;
            this.name = name;
        }
    }
}
