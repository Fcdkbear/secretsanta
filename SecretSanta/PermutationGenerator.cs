﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretSanta
{
    class PermutationGenerator
    {
        int n;
        private Random rnd;

        public PermutationGenerator(int n)
        {
            rnd = new Random(Environment.TickCount);
            this.n = n;
        }

        private bool isPermutationGood(int[] permutation)
        {
            for (int i = 0; i < permutation.Length; ++i)
            {
                if (permutation[i] == i)
                {
                    return false;
                }
            }
            return true;
        }

        private int[] generateRandomPerumutation()
        {
            int[] result = new int[n];
            for (int i = 0; i < n; ++i)
            {
                result[i] = i;
            }
            result = result.OrderBy(x => rnd.Next()).ToArray();
            return result;
        }

        public int[] generateGoodPermutaion()
        {
            int[] result = new int[n];
            do
            {
                result = generateRandomPerumutation();
            } while (!isPermutationGood(result));
            return result;
        }
    }
}
