﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecretSanta
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int numberOfParticipants = Convert.ToInt32(participantsNumberTextBox.Text);
            string[] information = informationTextBox.Text.Split(new char[] { ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            Person[] persons = new Person[numberOfParticipants];
            for (int i = 0; i < numberOfParticipants; ++i)
            {
                persons[i] = new Person(information[i * 2 + 1], information[i * 2]);
            }
            int[] permutation = new PermutationGenerator(numberOfParticipants).generateGoodPermutaion();
            EmailSender emailSender = new EmailSender();
            progressBar.Maximum = 100;
            for (int i = 0; i < numberOfParticipants; ++i)
            {
                emailSender.sendEmail(persons[i], persons[permutation[i]]);
                Thread.Sleep(1000);
                progressBar.Value = (i + 1) * 100 / numberOfParticipants;
            }
        }
    }
}
